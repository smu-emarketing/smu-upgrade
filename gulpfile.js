var gulp        = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass        = require('gulp-sass');

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {

    return gulp.src("./src/scss/**/**/*.scss")
        //.pipe(sass({outputStyle: 'nested', sourceComments: 'map'}))
        .pipe(sass({outputStyle: 'nested'}))
        .pipe(gulp.dest("./public/css"))
        .pipe(browserSync.stream());

});

// Static Server and watch files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./public",
        notify: false
    });

    gulp.watch("./src/scss/**/*.scss", ['sass']);
    gulp.watch("./public/**/*.html").on("change", browserSync.reload);

});

gulp.task('default', ['serve']);
