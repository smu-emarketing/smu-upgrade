$(document).ready(function(){
  if ($(".site-alert").children().length > 0) {
    $(".site-alert").addClass('show');
  }
});

//move cursor to search form on click of search icon
$('.search.dropdown').on('shown.bs.dropdown', function () {
  // do something…
  $('.smu-search-input').focus();
})

//$('.nav-collapse').collapse();

$('.carousel').carousel({
  interval: 5000
});
